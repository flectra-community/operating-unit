# Copyright 2021 Ecosoft Co., Ltd. (http://ecosoft.co.th)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Access all OUs' Expenses",
    "version": "2.0.1.0.0",
    "author": "Ecosoft,Odoo Community Association (OCA)",
    "category": "Generic Modules/Human Resources",
    "license": "AGPL-3",
    "website": "https://gitlab.com/flectra-community/operating-unit",
    "depends": ["hr_expense_operating_unit"],
    "data": [
        "security/hr_expense_security.xml",
    ],
    "installable": True,
    "maintainers": ["kittiu"],
}
