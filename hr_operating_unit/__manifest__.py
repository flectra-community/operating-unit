# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "HR Operating Unit",
    "version": "2.0.1.1.0",
    "author": "Gonzalo González Domínguez," "Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "website": "https://gitlab.com/flectra-community/operating-unit",
    "category": "Human Resources",
    "depends": ["hr", "operating_unit"],
    "data": [
        "security/security.xml",
        "views/hr_employee.xml",
    ],
    "installable": True,
}
