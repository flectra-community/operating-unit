# Copyright 2021 Ecosoft Co., Ltd. (http://ecosoft.co.th)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Access all OUs' Stock",
    "version": "2.0.1.0.1",
    "author": "Ecosoft,Odoo Community Association (OCA)",
    "category": "Warehouse Management",
    "license": "AGPL-3",
    "website": "https://gitlab.com/flectra-community/operating-unit",
    "depends": ["stock_operating_unit"],
    "data": [
        "security/stock_security.xml",
    ],
    "installable": True,
    "maintainers": ["kittiu"],
}
