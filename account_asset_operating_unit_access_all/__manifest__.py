# Copyright 2021 Ecosoft Co., Ltd. (http://ecosoft.co.th)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Access all OUs' Assets",
    "version": "2.0.1.0.0",
    "author": "Ecosoft, Odoo Community Association (OCA)",
    "category": "Accounting & Finance",
    "license": "AGPL-3",
    "website": "https://gitlab.com/flectra-community/operating-unit",
    "depends": ["account_asset_operating_unit"],
    "data": [
        "security/account_asset_security.xml",
    ],
    "installable": True,
    "development_status": "Alpha",
    "maintainers": ["ps-tubtim"],
}
