# © 2015-17 ForgeFlow S.L.
# - Jordi Ballester Alomar
# © 2015-17 Serpent Consulting Services Pvt. Ltd. - Sudhir Arya
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl.html).

{
    "name": "Operating Unit in Purchase Orders",
    "summary": "Adds the concecpt of operating unit (OU) in purchase order "
    "management",
    "version": "2.0.1.0.2",
    "author": "ForgeFlow, "
    "Serpent Consulting Services Pvt. Ltd.,"
    "Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/operating-unit",
    "category": "Purchase Management",
    "depends": ["stock_operating_unit", "purchase_stock"],
    "license": "LGPL-3",
    "data": [
        "security/purchase_security.xml",
        "views/purchase_order_view.xml",
        "views/purchase_order_line_view.xml",
    ],
    "demo": ["demo/purchase_order_demo.xml"],
    "installable": True,
}
